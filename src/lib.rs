use serde::{Serialize, Deserialize};

pub type Name = String;
pub type Score = i64;

#[derive(Serialize,Deserialize,PartialEq,Eq)]
pub enum Tile {
    Bamboo(u8),
    Characters(u8),
    Dots(u8),
    Dragon(Dragon),
    Wind(Wind),
    Flower(Wind),
    Season(Wind)
}

#[derive(Serialize,Deserialize,PartialEq,Eq)]
pub enum Wind {
    North,
    East,
    South,
    West
}

#[derive(Serialize,Deserialize,PartialEq,Eq)]
pub enum Dragon {
    Red,
    Green,
    White
}


#[derive(Serialize,Deserialize)]
pub enum ClientMessage {

    // In-game commands
    Draw,
    Discard(Tile),
    Kong,
    Chow,
    ExplicitChow(Tile,Tile),
    Pung,
    Mahjong,

    // Meta commands
    //
    // Send a text chat
    Say(String),

    // Create/join a game with a given lobby name and password
    New(String,String),
    Join(String,String),
    ListRooms,

    // Leave the room you're in
    Leave,

    // Quit the entire game
    Quit,

    // Changes the user's nickname
    Nick(Name),
}

#[derive(Serialize,Deserialize)]
pub enum ServerMessage {
    // A text chat from another user
    Say(Name,String),

    // In-game actions
    Draw(Name),
    Discard(Name,Tile),
    // Kongs and pungs are all the same so we only need to send one tile
    Kong(Name,Tile),
    Pung(Name,Tile),
    Chow(Name,Tile,Tile,Tile),
    // Theoretically the winning tiles could be enumerated here
    // but that'd be incredibly painful to use
    Mahjong(Name,Vec<Tile>),

    // List the scores and winds of active players
    Score(Vec<(Name,Score)>),
    Winds(Vec<(Name,Wind)>),

    // List the tiles remaining in the deck
    RemainingTiles(u8),

    // Mark when a new player has joined
    Join(Name,Wind),

    // Mark when a game is over
    GameOver,

    // Actions that change your tiles
    InitialHand(Vec<Tile>),
    DrawnTile(Tile),

    // Response to the ListRooms command
    Rooms(Vec<String>)
}
